(function($) {

  function get_field_value(fieldset, field) {
    var fieldset_id = "edit-" + fieldset.replace(/_/g, '-'); 
      var field_id = "edit-" + field.replace(/_/g, '-'); 
      var field_name = field;
      var common_prefix = '';
      var field_not_empty = 0;
      var fields_found = $("#"+fieldset_id).find("[id^="+field_id+"][name^="+field_name+"]");
      $(fields_found).each( function(index) {
        if( index == 0 ) {
          var lang_suffix = this.id.replace(field_id, "").replace(/(-[^-]+).*/,"$1");
          common_prefix = field_id + lang_suffix;
        }
        if( this.id.indexOf(common_prefix) === 0 ) {		
        var this_type = $(this).attr('type');
        if( this_type == "text" || this_type == "textarea" ) { 
          field_not_empty += $(this).val().trim() ? 1 : 0; 
  
        } else if( this_type == "select-one" ) {
          field_not_empty += $(this).val() == "_none" ? 0 : ( $(this).val() ? 1 : 0 ); 
  
        } else if( this_type == "select-multiple" ) { // to be tested...
          field_not_empty += ( !$("#"+this.id+" option:selected").length ) ? 0 : 1;
  
        } else if( this_type == "checkbox" || this_type == "radio" ) {
          field_not_empty += $(this).is(':checked') ? 1 : 0;
  
        } 
        // make sure any changes will update the completion rate again
          $(this).unbind('change.fieldsetCompletion_'+fieldset);  
          $(this).bind('change.fieldsetCompletion_'+fieldset, function() {
          update_fieldset_summary(fieldset);
        });

      } 
    });
    if( common_prefix == '' ) {
      return -1;
    }
    return field_not_empty;
  }

  function calc_fieldset_values(fieldset, rules_arr) {
    var rules_arr_values = [];
    
    for( var i = 0; i < rules_arr.length; i++ ) {
      var and_arr = rules_arr[i];
      var new_arr = [];
      for( var j = 0; j < and_arr.length; j++ ) {
  
        if( typeof and_arr[j] === 'string' ) {
          var val = get_field_value(fieldset, and_arr[j]);
          if( val >= 0 ) {
            new_arr.push( and_arr[j] + ':' + val );
          }
        } else {
          new_arr.push( calc_fieldset_values(fieldset, and_arr[j]) );
        }
      }
      if( new_arr.length > 0 ) {
        rules_arr_values.push( new_arr ); 
      }
    }
    return rules_arr_values;
  }

  function calc_fieldset_completion(fieldset, rules_arr, max_value) {
    max_value = typeof max_value !== 'undefined' ? max_value : 1.0;
    var value = 0.0;
    for( var i = 0; i < rules_arr.length; i++ ) {
      var and_arr = rules_arr[i];
      var act_value = max_value / and_arr.length;
      var and_value = 0.0;
      for( var j = 0; j < and_arr.length; j++ ) {
        if( typeof and_arr[j] === 'string' ) {
          var val = and_arr[j].split(":")[1] > 0 ? 1 : 0;
          and_value = and_value + act_value * val;
        } else {
          and_value = and_value + calc_fieldset_completion(fieldset, and_arr[j], act_value);
        }
      }
      value = and_value > value ? and_value : value; 
    }
    return value;
  }
  
  function update_fieldset_summary(fieldset, rules_arr) {
    var fieldset_id = "edit-" + fieldset.replace(/_/g, '-'); 
    if( typeof rules_arr === 'undefined' ) { 
      var completion_rules = jQuery.parseJSON(Drupal.settings.fieldsetCompletion.completion_rules);
      rules_arr = completion_rules[fieldset];
    } 

    var fieldset_id = "edit-" + fieldset.replace(/_/g, '-'); // alert(fieldset_id);
    var rules_arr_values = calc_fieldset_values(fieldset, rules_arr);
    var completion = Math.round(100 * calc_fieldset_completion(fieldset, rules_arr_values));
    var colour_class = completion < 26 ? 'fs_red' : completion < 46 ? 'fs_red_light' : completion < 71 ? 'fs_yellow' : completion < 86 ? 'fs_green_light' : 'fs_green';

    jQuery('#'+fieldset_id+' span.fieldset-legend span.summary:first').addClass('fieldset-summary fs_right_span').removeClass('summary');
    jQuery('#'+fieldset_id+' span.fieldset-summary:first').html(completion+'%');
    jQuery('#'+fieldset_id+' span.fieldset-summary:first').attr('title', 'Completion');
    jQuery('#'+fieldset_id+' span.fieldset-summary:first').removeClass('fs_red fs_red_light fs_yellow fs_green_light fs_green').addClass(colour_class);
  }

  function update_all_fieldset_summaries() {
    // do completion 	
    var completion_rules = jQuery.parseJSON(Drupal.settings.fieldsetCompletion.completion_rules);

    jQuery.each(completion_rules, function(fieldset, rules_arr) {
      update_fieldset_summary(fieldset, rules_arr);	 
    }); 
  }

  Drupal.behaviors.fieldsetCompletion = {
    attach: function(context, settings) {
      update_all_fieldset_summaries();
    }
  };
  
})(jQuery); 