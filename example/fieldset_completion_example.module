<?php
/**
 * @file
 * Example module for how to use the fieldset completion module
 */
 
/**
 * Implements hook_help()
 */
function fieldset_completion_example_help($path, $arg) {
  switch ($path) {
    case "admin/help#bb_helper":
      $output = '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Example module for how to use the fieldset completion module.') . '</p>';
      return $output;
      break;
  }
}
 

/**
 * Implements hook_form_alter()
 *
 * Used to add some fields and a fieldset with completion rate settings
 */
function fieldset_completion_example_form_alter(&$form, $form_state, $form_id) {

  if( $form_id == 'article_node_form' ) {
    
    $form['body']['#weight'] = 4;
    $form['field_image']['#weight'] = 5;

    $selected = isset($form_state['values']['topics']) ? $form_state['values']['topics'] : '_none';
    $form['topics'] = array(
      '#type' => 'select',
      '#title' => t('Topic'),    
      '#options' => array( '_none' => '- Select -', 'fruits' => 'Fruits', 'baking' => 'Baking', 'car' => 'Car', ),
      '#default_value' => $selected,
      '#ajax' => array(
        'event' => 'change',
        'wrapper' => 'dependent_activities_wrapper',
        'callback' => 'fieldset_completion_example_dependent_activities_callback',
        'method' => 'replace',      
      ),
      '#weight' => 0,
    );
    
    $form['activities'] = array(
      '#type' => 'item',
      '#prefix' => '<div id="dependent_activities_wrapper">',
      '#suffix' => '</div>',    
      '#weight' => 1,
    );
    if( $selected != '_none' ) {
      $options = fieldset_completion_example_get_activities_options($selected);
      $form['activities']['#type'] = 'checkboxes';
      $form['activities']['#title'] = t('Activities');
      $form['activities']['#options'] = $options;
    }
    
    // now create a fieldset for some fields...
    $form['fieldset_info'] = array(
      '#type' => 'fieldset',
      '#title' => t('Info section'),
      '#weight' => 0,
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $form['fieldset_info']['field_tags'] = $form['field_tags']; unset($form['field_tags']);
    $form['fieldset_info']['topics'] = $form['topics']; unset($form['topics']);
    $form['fieldset_info']['activities'] = $form['activities']; unset($form['activities']);
    
    // ...and specify the completion rules for our fields
    $settings['completion_rules'] = json_encode( fieldset_completion_make_fieldset_rules( array(
      'fieldset_info' => 'field_tags & topics & activities',
    ) )); 
    $path = drupal_get_path('module', 'fieldset_completion'); 
    $form['#attached']['css'][] = array(
      'data' => $path.'/fieldset_completion.css', array('group' => CSS_DEFAULT, 'type' => 'file'),
    ); 
    $form['#attached']['js'][] = $path.'/fieldset_completion.js';
    $form['#attached']['js'][] = array(
      'data' => array('fieldsetCompletion' => $settings),
      'type' => 'setting',
    );     
  }
}


/**
 * Process the ajax callback for the dependent activities field
 */
function fieldset_completion_example_dependent_activities_callback($form, $form_state) {
  return $form['fieldset_info']['activities'];
}


/**
 * Get the dependent options for the activities field
 */
function fieldset_completion_example_get_activities_options($selected) {
  if($selected == 'car') {
    return array( 'windows' => 'Clean windows', 'gas' => 'Fill up gas', 'tires' => 'Check tire pressure', );
    
  } elseif($selected == 'baking') {
    return array( 'croissants' => 'Make croissants', 'ciabatta' => 'Bake ciabatta', 'chocolate_cake' => 'Prepare chocolate cake', );
    
  } else {
    return array( 'apples' => 'Eat apples', 'bananas' => 'Peel bananas', 'oranges' => 'Squeeze oranges', );
  } 
}


