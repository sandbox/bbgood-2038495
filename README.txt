fieldset_completion
===================

The fieldset_completion module (Drupal 7) is an example of how you can use jQuery/javascript to show a completion rate for all the input fields in a fieldset. It grabs the fieldset span.summary tag (inside the span.fieldset-legend) to display the completion rate.

The core functionality is in the function fieldset_completion_make_fieldset_ruleparser() in PHP and its counterpart calc_fieldset_values() in javascript which does a very simple parsing of completion rules of the sort 'title & tags & body' (meaning all of these fields must have a value for a 100% completion rate).

Settings:

Setup your fieldsets for example in hook_form_alter() and specify your completion rules for each fieldset in the $settings['completion_rules'] array as shown in the example. 

Here is a valid example.

  $settings['completion_rules'] = json_encode( fieldset_completion_make_fieldset_rules( array(
    'fieldset_profile' => 'title & field_lu_unit & field_lu_unit_topic',
    'fieldset_objectives' => 'field_lu_objective_1 | field_lu_objective_2 | field_lu_objective_3 | field_lu_objective_4 | field_lu_objective_5',
    'fieldset_activities' => '( field_ut_objective_1 | field_ut_objective_2 | field_ut_objective_3 ) & field_ut_communication & field_ut_cultures & field_ut_connections',
  ) ));

So here we have three fieldsets 'fieldset_profile', 'fieldset_objectives' and 'fieldset_activities', and for each we define which fields need to be filled in by the user for a 100% completion of that fieldset. 

The code is still experimental, improvements are welcome. Thank you.
